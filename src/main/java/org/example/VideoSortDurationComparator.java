package org.example;

import java.util.Comparator;

public class VideoSortDurationComparator implements Comparator<Video> {
    @Override
    public int compare(Video sortVideoLike1, Video sortVideoLike2) {
        return Integer.compare(sortVideoLike1.duration, sortVideoLike2.duration);

    }
}
