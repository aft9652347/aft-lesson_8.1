package org.example;

import java.util.Objects;

public class Video implements Comparable<Video> {

    private static int totalNumberOfVideos;
    private static int totalDurationOfTheVideo;

    String author;
    String title;
    String comments;
    Integer duration;

    //ПЕРЕОПРЕДЕЛЕНИЕ МЕТОДА ТУСТРИНГ ДЛЯ КРАСИВОГО ВЫВОДА
    @Override
    public String toString() {
        return "\nVideo{" +
                "author=" + author +
                " title=" + title +
                " comments=" + comments +
                " duration=" + duration+"}";
    }
    //Переопределение метода кампареТу для сортировки
    @Override
    public int compareTo(Video sortVideoDuration) {
        return this.duration- sortVideoDuration.duration;
    }
    //Переопределение метода иклс для сравнения
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Video video = (Video) o;
        return this.author.equals(video.author)
                && this.title.equals(video.title)
                && this.comments.equals(video.comments)
                && this.duration == video.duration;
    }
    //Переопределение метода хэшКод для сравнения что побыстрее
    @Override
    public int hashCode() {
        return Objects.hash(author, title, comments, duration);
    }

    //Конструктор для создания объектов видео
    private Video(String author, String title, String comments, Integer duration){
        this.author = author;
        this.title = title;
        this.comments = comments;
        this.duration = duration;

        totalNumberOfVideos++;
        totalDurationOfTheVideo = totalDurationOfTheVideo + duration;
    }
    //Создания объектов видео
    static Video cat = new Video("jo", "cat and home", "WOW", 13000);
    static Video dog = new Video("fuo", "dog ", "Super", 483000);
    static Video superBus = new Video("Лютый Серж", "Автобус в полёте", "Ничёси", 788513000);
    static Video smokeFish = new Video("Рыбак", "Туманная рыба", "Где такую поймать?", 589657);
    static Video smokeFish2 = new Video("Рыбак", "Туманная рыба2", "Ого, я балдею", 89965);
    static Video dogAndCat = new Video("fuji","Кошка с собакой","Вот так милота",8785445);


}
