package org.example;

import java.util.Iterator;

public class Main extends Account {
    public static void main(String[] args) {

        //Создаём объект account класса Account для работы с его нестатическими методами
        Account account = new Account();

        //Добавление понравившегося видео в список понравившихся
        Account.addLikeVideo(Video.cat);
        Account.addLikeVideo(Video.dog);
        Account.addLikeVideo(Video.smokeFish);
        Account.addLikeVideo(Video.superBus);
        Account.addLikeVideo(Video.dogAndCat);
        Account.addLikeVideo(Video.smokeFish2);

        //System.out.println(Account.likeVideo);
        //Вывод на экран всех понравившихся видео (обход списка должен быть сделан через объект итератора)
        Iterator<Video> iterator = likeVideo.iterator();
        while (iterator.hasNext()) {
            Video video = iterator.next();
            System.out.println(video);
        }

        //Вывод списка всех понравившихся видео, отсортированных по возрастанию длительности видео
        // (сделать через Comparable)
        account.sortLikeVideoDurationComparable(likeVideo);
        System.out.println(Account.likeVideo);

        //Добавляем в целях наглядности последующей сортировки
        Account.addLikeVideo(Video.smokeFish2);
        Account.addLikeVideo(Video.smokeFish2);

        //- вывод списка всех понравившихся видео, отсортированных по возрастанию длительности видео
        // (сделать через Comparator)
        account.sortLikeVideoDurationComparator(Account.likeVideo, new VideoSortDurationComparator());

        //Добавить видео
        Account.addLikeVideo(Video.cat);
        Account.addLikeVideo(Video.cat);
        Account.addLikeVideo(Video.dog);
        Account.addLikeVideo(Video.superBus);
        Account.addLikeVideo(Video.dogAndCat);
        Account.addLikeVideo(Video.smokeFish2);
        Account.addLikeVideo(Video.smokeFish);
        //удаление видео из списка понравившегося
        Account.removeVideoFromLikeVideo(Video.dog);
        //Метод для сортировки на базе компаратора из анонимного класса
        account.sortLikeVideoDurationComparator2(likeVideo);
        System.out.println("\nВывод до удаления дублей:\n"+Account.likeVideo);
        
        //Удалить повторяющиеся элементы из списка
        removeTakeVideo();
        System.out.println("\nВывод после удаление дубликатов:\n"+Account.likeVideo);

        }
    }
