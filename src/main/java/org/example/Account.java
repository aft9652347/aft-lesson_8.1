package org.example;

import java.util.*;

import static javax.swing.UIManager.get;

public class Account {
    //Создание списка с понравившимися видео
    static List<Video> likeVideo = new LinkedList<>();
    //Создание метода на базе компарабел для сортировки по полю duration
    public void sortLikeVideoDurationComparable(List<Video> listSortLikeVideo){
    Collections.sort(listSortLikeVideo);
    }

    //Создание метода для сортировки по полю duration на базе компаратора
    public void sortLikeVideoDurationComparator(List<Video> sortVideoC, VideoSortDurationComparator sortDurationComparator){
        Collections.sort(sortVideoC, sortDurationComparator);
    }

    //Метод для добавления понравившихся видео в список с такими видео
    public static void addLikeVideo(Video newVideo) {
        likeVideo.add(newVideo);
    }
    //Удаление видео из списка понравившихся видео
    public static void removeVideoFromLikeVideo(Video removeVideo){
        likeVideo.remove(removeVideo);
    }

    //Метод для удаления дублей. Если хэши совпадают смотрим на иклс если совпали удалить
    public static void removeTakeVideo(){
        for (int i = 0; i < likeVideo.size(); i++) {
            Video a1 = likeVideo.get(i);
            for (int j = 0; j < likeVideo.size(); j++) {
                if (i==j){
                    continue;
                }
            if(a1.hashCode() == likeVideo.get(j).hashCode()){
                if (a1.equals(likeVideo.get(j))){
                    likeVideo.remove(j);
                    j--;
                }
            }
            }
        }
        //Можно избавится от дубликатов через перевод в HasSet
//        Set<Video> set = new HashSet<>(likeVideo);
//        likeVideo.clear();
//        likeVideo.addAll(set);
//        System.out.println(set);
    }


    //компаратор на базе анонимного класса
    Comparator<Video> comparatorDuration = new Comparator<Video>() {
        @Override
        public int compare(Video o1, Video o2) {
            return Integer.compare(o1.duration, o2.duration);
        }
    };
    //Метод для сортировки с компаратором на базе анонимного класса
    public void sortLikeVideoDurationComparator2(List<Video> sortVideoC2){
        Collections.sort(sortVideoC2, comparatorDuration);
    }
}
